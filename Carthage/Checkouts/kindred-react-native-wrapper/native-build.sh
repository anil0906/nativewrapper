#!/bin/bash

#pre-clean up code here for web folder

#rm -rf web/js/ && echo "Removed content from web folder"


cd ../kindred-rc && echo "Changed directory to kindred-rc"

#clean directories
#rm -rf dist/ node_modules/ && echo "Removed node_modules and dist folder"

#install
#yarn && echo "Yarn install completed"

#build
#yarn webpack-build --env.app_version native && echo "webpack-build completed"

#copy build resources to web folder
#cp -r dist/native/ ../kindred-react-native-wrapper/web/js/ && echo "copied resources to web folder"

#Create env file based on build type and env
cd ../kindred-react-native-wrapper
#reading arguments
buildRegion=$1 # 'AU' | 'UK'
env=$2 # 'integration' | 'qa' | 'production'
case "$buildRegion" in
    "AU")
        domain="com.au"
        ;;
    "UK")
        domain="co.uk"
        ;;
esac

case "$env" in
    "qa")
        environment="-qa"
        ;;
    "integration")
        environment="-integration"
        ;;
    "production")
        environment=""
        ;;
esac
file=web/js/env.js
rm -rf $file
echo "window.__BUILD_REGION__ = '$buildRegion';" >> $file
echo "window.__API_HOST__ = 'https://rsa$environment.unibet.$domain';" >> $file
echo "window.__GRAPHQL_HOST__ = 'https://rsa$environment.unibet.$domain/api/v1/graphql';" >> $file
echo "window._rcConfig = {elementToRenderIn: 'krc-container'};" >> $file




