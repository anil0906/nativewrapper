//
//  KRCMainViewController.swift
//  KRCFramework
//
//  Created by Anil Sharma on 3/12/19.
//  Copyright © 2019 Anil Sharma. All rights reserved.
//

import UIKit
import WebKit

public protocol KRCViewControllerDelegate: AnyObject {
    /// Everytime KRCFramework wants to invoke a login screen should invoke this method from the delegate:
    /// example:
    ///      krcViewController.delegate?.racingClientDidRequestLogin(krcViewController)
    func racingClientDidRequestLogin(_ invoker : KRCMainViewController)
}

public protocol KRCViewController: UIViewController {
    /// The delegate injected from outside. Needed to invoke a login screen
    var delegate: KRCViewControllerDelegate? { get set }
    
    /// Login method using a token. Everytime we do a login, we invoke this method to communicate KRCFramework that the login was successful
    func login(token: String)
    
    /// Logout method. Everytime we do a logout, we invoke this method to communicate KRCFramework that the user did Log out
    func logout()
}

open class KRCMainViewController: UIViewController, KRCViewController {
    
    open weak var delegate: KRCViewControllerDelegate? = nil
    
    open var webView: WKWebView!
    
    override open func loadView() {
        let contentController = WKUserContentController()
        // CAST GC token will be passed through delegate
        let token = "CASTGC_LOGOUT=TGT-26601-xiYOvNekRyq0KDngftBimIYMrCDIaxKwGRcHUSc4BBvfgZHZeJ-cas"
        self.login(token: token)
        // Can be used to pass initial info like rc config or deep linking url
        let scriptSource = "window.urlToBeLoaded = `/#/eventPage`"
        let script = WKUserScript(source: scriptSource, injectionTime: .atDocumentStart, forMainFrameOnly: true)
        contentController.addUserScript(script)
        webView = makeWebView()
        webView.configuration.userContentController = contentController
        view = webView
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        loadStaticPage()
    }
    
    open func makeWebView() -> WKWebView {
        return WKWebView(frame: .zero, configuration: WKWebViewConfiguration())
    }
    
    /// We do not know what is the proper method for logic so we offer a sample implementation
    open func login(token: String) {
        
        //Cookie
        let cookieHeaderField = ["Set-Cookie": token]
        
        // gamelauncher url
        // Resolve additional params
        if let gameLauncherUrl = URL(string: "https://www-qa1.unibet.co.uk/ugracing-rest-api/gameLauncher.json?gameId=FrankelLobby&uniqueGameId=FrankelLobby%40ugracing&locale=en_GB&brand=unibet&currency=GBP&clientId=polopoly_mobilephone-android&deviceGroup=mobilephone&deviceOs=android&jurisdiction=UK&useRealMoney=true&marketLocale=en_GB&_=1579754851607") {
            let cookies = HTTPCookie.cookies(withResponseHeaderFields: cookieHeaderField, for: gameLauncherUrl)
            let cookieStorage = HTTPCookieStorage.shared
            cookieStorage.setCookies(cookies, for: gameLauncherUrl, mainDocumentURL: gameLauncherUrl)
            let task = URLSession.shared.dataTask(with: gameLauncherUrl) {(data, response, error) in
                guard let data = data else { return }
                // Ticket to be passed in webview when resolved
                print(String(data: data, encoding: .utf8)!)
            }
            
            task.resume()
            
        }
    }
    
    open func logout() {
        
    }
    
}

private extension KRCMainViewController {
    private func loadStaticPage() {
        if let htmlPath = Bundle(for: KRCMainViewController.self).path(forResource: "web/index", ofType: "html") {
            let url = URL(fileURLWithPath: htmlPath)
            let request = URLRequest(url: url)
            webView.load(request)
        } else {
            // show error page
        }
    }
}
